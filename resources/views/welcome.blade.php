
@extends('layout.app')

@section('content')
    <div class="full-height">
        <div class="content">
            <div class="title m-b-md">
                Welcome to UFIS-BNB!
            </div>
            <div class="links">

                <div style="padding-left: 700px">
                    {{ Form::open(['url'=>'/property/types', 'method'=>'POST']) }}
                    <select class="browser-default custom-select mb-2" name="propertyType" id="propertyType">
                        @if(!empty($propertyType))
                            <option value="" disabled> Select Propperty Type </option>
                            <option value="All" <?php if(($propertyType=='All')) echo "selected";?>> All Properties </option>
                            <option value="House" <?php if(($propertyType=='House')) echo "selected";?>> House </option>
                            <option value="Apartment" <?php if(($propertyType=='Apartment')) echo "selected";?>> Apartment </option>
                            <option value="Cabin" <?php if(($propertyType=='Cabin')) echo "selected";?>> Cabin </option>

                        @else
                            <option value="" disabled> Select Propperty Type </option>
                            <option value="All"> All Properties </option>
                            <option value="House"> House </option>
                            <option value="Apartment"> Apartment </option>
                            <option value="Cabin"> Cabin </option>
                        @endif

                    </select>

                    {{ Form::close()}}

                    <br>

                    <a href="{{route('sort') }}">
                        <button type="button" style="background: yellowgreen">
                            Sort Property By Rating
                        </button>
                    </a>
                </div>

                <br>


                @if(!empty($sortedData))

                    <div class="center">
                        <table>

                            <tr>
                                <th>Properties</th>
                                <th>Average Rating</th>
                            </tr>

                            @foreach($sortedData as $sortedDataKey => $sortedDataValue)
                                <tr>
                                    <td><a href="{{ route('property-details', ['id' => $sortedDataValue->property_id]) }}">{{ $sortedDataValue->title }}</a></td>

                                    @if(is_float($sortedDataValue->avg_rate))
                                        {{--                                        <td>{{number_format((float)$sortedDataValue->avg_rate, 2, '.', '')}}</td>--}}
                                        <td>{{round($sortedDataValue->avg_rate,2)}}</td>
                                    @else
                                        <td>{{$sortedDataValue->avg_rate}}</td>
                                    @endif

                                </tr>
                            @endforeach
                        </table>
                    </div>

                @else
                    @foreach($properties as $property)
                        <a href="{{ route('property-details', ['id' => $property->property_id]) }}">{{ $property->title }}</a>
                    @endforeach
                @endif


            </div>

            <br>

        </div>

    </div>
    </div>
@endsection





