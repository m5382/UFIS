@extends('layout.app')

@section('content')
 <div class="content">
     <h1> {{$details[0]->title}} </h1>
     <h2>Type of Property: {{$details[0]->type}} </h2>
       <br>

     <br>



     <div class="center">
         <h1>Reviews </h1>
         <table>
             <tr>
                 <th>Rating</th>
                 <th>Comments</th>
             </tr>


             @foreach ($details as $detailKey => $detailValue)
                 <tr>
                     <td>{{$detailValue->Rating}}</td>
                     <td>{{$detailValue->Comment}}</td>
                </tr>
             @endforeach

         </table>

     </div>
 </div>

@endsection
