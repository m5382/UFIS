## Setup ##

#### Docker Setup ####

* Rename `docker-compose-mssql.yml` to `docker-compose.yml`
* Run `docker-compose up` 
* In your Database UI(I have one on my PhpStorm)(or something like datagrip can be used), create a database name `ufis1`.
* I am also posting the pictures for the same to create Database in the Repo

#### Other Setup ####
* Clone the repo
* In terminal run `composer install`
* In terminal run `php artisan migrate:fresh --seed`
* In terminal run `php artisan serve`
* In your browser run `http://localhost:8000/`

## Tasks Achieved ##

* [x] Display all properties from the `Property` table on the homepage as links (this was done for you) 
* [x] Assign types to the properties  **HINT: Is a new table needed for this?**
     * [x] Properties with `property_id` 1 and 2 should have type: "House"
     * [x] Properties with `property_id` 3 and 4 should have type: "Apartment"
     * [x] Property with `property_id` 5 should have type: "Cabin"
* [x] Create fake reviews for some (or all) of the properties. Reviews should include a numeric rating from 1-5 (required) as well as a comment (optional).
* [x] Route each link from the homepage to a separate page containing details for each of the properties. The property pages should include: the title of the property, its type, and its reviews.
* [x] Allow users to filter all properties on the homepage by type.
* [x] Allow users to sort all properties on the homepage by average rating.
