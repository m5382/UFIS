<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;


class InsertPropertyTypeId_PropertyTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		DB::table('property')->where('property_id', '1')->update(['property_type_id'=>1]);
		DB::table('property')->where('property_id', '2')->update(['property_type_id'=>1]);
		DB::table('property')->where('property_id', '3')->update(['property_type_id'=>2]);
		DB::table('property')->where('property_id', '4')->update(['property_type_id'=>2]);
		DB::table('property')->where('property_id', '5')->update(['property_type_id'=>3]);
	}
}
