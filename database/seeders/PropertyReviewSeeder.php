<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;


class PropertyReviewSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('property_reviews')->insert([
				['property_id' => 1, 'Rating' => '2', 'Comment' => 'needs more cleaning'],
				['property_id' => 2, 'Rating' => '4', 'Comment' => 'Great Realtor'],
				['property_id' => 3, 'Rating' => '3', 'Comment' => 'Beyond my expectations'],
				['property_id' => 4, 'Rating' => '4.1', 'Comment' => 'Beautiful Property'],
				['property_id' => 4, 'Rating' => '2', 'Comment' => 'Staff was rude'],
				['property_id' => 5, 'Rating' => '5', 'Comment' => 'Excellent staff and clean property'],

				['property_id' => 1, 'Rating' => '2.4', 'Comment' => 'Average Property'],
				['property_id' => 2, 'Rating' => '5', 'Comment' => 'Loved It'],
				['property_id' => 3, 'Rating' => '2', 'Comment' => 'Not good'],
				['property_id' => 4, 'Rating' => '3', 'Comment' => 'Its nice'],
				['property_id' => 4, 'Rating' => '4', 'Comment' => 'Nice swimming pool'],
				['property_id' => 5, 'Rating' => '2', 'Comment' => 'Its not nice'],

				['property_id' => 1, 'Rating' => '5', 'Comment' => 'Just Wow!!'],
				['property_id' => 2, 'Rating' => '2', 'Comment' => 'Not as expected'],
				['property_id' => 3, 'Rating' => '4', 'Comment' => 'Close to college'],
				['property_id' => 4, 'Rating' => '5', 'Comment' => 'Mesmerizing'],
				['property_id' => 4, 'Rating' => '3', 'Comment' => 'Bad neighborhood'],
				['property_id' => 5, 'Rating' => '5', 'Comment' => 'Awesome view'],

				['property_id' => 1, 'Rating' => '1', 'Comment' => 'There is water problem in my apartment'],
				['property_id' => 2, 'Rating' => '2', 'Comment' => 'Heating doesnt work as expected'],
				['property_id' => 3, 'Rating' => '5', 'Comment' => 'Nice to be here'],
				['property_id' => 4, 'Rating' => '2', 'Comment' => 'Overly priced'],
				['property_id' => 4, 'Rating' => '2.5', 'Comment' => 'Wont recommend to anyone'],
				['property_id' => 5, 'Rating' => '3', 'Comment' => 'I like it here but staff is rude']

			]
		);

	}
}
