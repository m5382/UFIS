<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(PropertySeeder::class);
         $this->call(PropertyTypeSeeder::class);
         $this->call(InsertPropertyTypeId_PropertyTableSeeder::class);
         $this->call(PropertyReviewSeeder::class);

    }
}
