<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPropertyTypeIdToPropertyTable extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::table('property', function($table) {
			$table->integer('property_type_id')->nullable();
			$table->foreign('property_type_id')->references('id')->on('property_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('property', function($table) {
			$table->dropForeign(['property_type_id']);
			$table->dropColumn('property_type_id');
		});
	}
}
