<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Property extends Model{
	protected $table = 'property';
	protected $primaryKey = 'id';


	public function __construct(array $attributes = [], $id = null)
	{
		parent::__construct($attributes);

		$this->primaryKey = $id;
	}

	public function getPropertyDetails(){

		return DB::select("SELECT p.title, pt.type, pr.Rating, pr.Comment FROM property p
								INNER JOIN property_type pt ON p.property_type_id = pt.id
								INNER JOIN property_reviews pr ON p.property_id = pr.property_id
								WHERE p.property_id=$this->primaryKey");

	}


	public function getPropertyByType($propertyType){

		switch ($propertyType) {
			case 'All':
				$properties = DB::select("SELECT property_id, title FROM property");
				break;

			default:
				$properties = DB::select("
										SELECT property_id, title FROM property WHERE property_type_id =
			                             (
			                                 SELECT id FROM property_type
			                                 WHERE type = '$propertyType'
			                            )"
				);
				break;
		}

		return $properties;
	}


	public function getPropertyByRating(){
		 return DB::select("
							SELECT pr.property_id,p.title, convert(varchar,cast(avg(cast(Rating AS DECIMAL(2,1))) AS DECIMAL(2,1)),1) AS avg_rate
							FROM property_reviews pr INNER JOIN property p
							ON pr.property_id=p.property_id
							GROUP BY pr.property_id, p.title
							ORDER BY avg_rate
		");

	}


}
