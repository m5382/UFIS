<?php

namespace App\Http\Controllers;

use App\Models\Property;
use DB;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
	/**
	* Show the profile for a given user.
	*
	* @param  int  $id
	* @return \Illuminate\View\View
	*/
	public function index($id)
	{
		$propertyModel = new Property([],$id);
		$details = $propertyModel->getPropertyDetails();

		return view('property.details', [
		'details'=>$details
		]);
	}


	public function types(Request $request){

		$propertyModel = new Property();
		$propertyType = $request->input('propertyType');

		$properties = $propertyModel->getPropertyByType($propertyType);

		return view('welcome', [
			'properties'=>$properties,
			'propertyType'=>$propertyType
		]);

	}


	public function sort(){
		$propertyModel = new Property();
		$sortedData = $propertyModel->getPropertyByRating();
		return view('welcome', [
			'sortedData'=>$sortedData,

		]);

	}
}
